#Description
* This image will load a config file and start tomcat.
* You can specify an $ARTIFACT_USERNAME/$ARTIFACT_PASSWORD to access $ARTIFACT_URL if needed (Basic http authentication).

#Usages:
* $ARTIFACT_URL=http://my-artifactory.com/artifactory/libs-snapshot-local/com/arx/origination/1.0.0-SNAPSHOT/origination-1.0.0-SNAPSHOT.war
* $ARTIFACT_USERNAME=username (optional)
* $ARTIFACT_PASSWORD=password (optional)

#Build:
docker build -t artifact-tomcat .

#Run:
docker run \ -e ARTIFACT_USERNAME=admin \ -e ARTIFACT_PASSWORD=xxx \ -e ARTIFACT_URL=http://artifactory.arxcf.com/artifactory/libs-snapshot-local/com/arx/origination/2.2.8-SNAPSHOT/origination-2.2.8-20181227.173947-134.war \ --name origination-tomcat \ -p 8080:8080/tcp -d origination-tomcat