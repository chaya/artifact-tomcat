#!/bin/bash

APP_PATH=/tmp/origination.war

if [ -e "$APP_PATH" ]; then
    echo 'App already installed, skipping installation'
else
    if [ -z "$ARTIFACT_URL" ]; then
        echo 'can'\''t download artifact, $ARTIFACT_URL parameter missing'
        exit 22
    fi

    AUTHORIZATION=""
    if [ -z "$ARTIFACT_USERNAME" ]; then
        echo 'No credentials found'
    else
        echo 'Using credentials ' ${ARTIFACT_USERNAME}':*'
        AUTHORIZATION="-u $ARTIFACT_USERNAME:$ARTIFACT_PASSWORD"
    fi

    echo 'Downloading artifact at' ${ARTIFACT_URL} 'using authorization:' ${AUTHORIZATION}
    curl $AUTHORIZATION $ARTIFACT_URL --output $APP_PATH

    echo 'Deploy war in tomcat'
    rm -rf /usr/local/tomcat/webapps/ROOT*
    cp $APP_PATH /usr/local/tomcat/webapps/ROOT.war
fi

echo 'Checking if we need to wait for a postgresql database to accept connections before starting'
if [ -z "$POSTGRES_HOST" ]; then
    echo 'No $POSTGRES_HOST specified skipping postgres database connection check'
else
    export PGHOST=${POSTGRES_HOST:='localhost'}
    export PGPORT=${POSTGRES_PORT:=5432}
    export PGUSER=${POSTGRES_USERNAME:='postgres'}
    export PGPASSWORD=${POSTGRES_PASSWORD:='manager'}
    until psql -c '\l';
    do
      >&2 echo "Postgres is unavailable - sleeping"
      sleep 5
    done
      >&2 echo "Postgres is up - continue startup"
fi

echo 'Launching app'
catalina.sh run